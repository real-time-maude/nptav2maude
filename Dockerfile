FROM --platform=linux/x86_64 ubuntu:22.04

LABEL maintainer="Jaime Arias <arias@lipn.univ-paris13.fr>"

# update install basic packages
RUN apt update && \
  apt install -y git wget python3 python3-pip

# install maude-se-yices
RUN wget -c https://tinyurl.com/3zttax25 -O - | tar -xz && \
  ln -s /maude-se-yices2/maude-se-yices2 /usr/local/bin/maude-se-yices2 && \
  echo "export MAUDE_LIB=/maude-se-yices2" >> ${HOME}/.bashrc

# install imitator
RUN wget -O /usr/local/bin/imitator https://github.com/imitator-model-checker/imitator/releases/download/v3.3.0/imitator-v3.3.0-linux-amd64 && \
  chmod +x /usr/local/bin/imitator

# change working directory
WORKDIR /app