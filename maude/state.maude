
***(
------------------
SYMBOLIC STATES 
------------------

Definition of states and operations on them

*)

load syntax .
load alt-smt .
load fme .


--- Definition of the state of a PTA: values for discrete variables and clocks
--- as well as the current location for each automaton
fmod VALUATION is
 pr NETWORK .
 pr FOURIER-MOTZKIN .

 --- From parameters, clocks, variables and locations to their current values
 sorts LocValuation ClockValuation ParValuation DVarValuation .

 --- Current location for a given automaton
 op _@_ : AutoId Location      -> LocValuation [ctor prec 10 format(g g g o)] .
 --- Current value for a clock
 op _:_ : Clock RExpr           -> ClockValuation [ctor prec 10] .
 --- Current value for a parameter
 op _:_ : Parameter RExpr       -> ParValuation [ctor prec 10] .
 --- Current value of a discrete variable
 op _:_ : DVariable RExpr       -> DVarValuation [ctor prec 10] .
endfm

view LocVal from TRIV to VALUATION is sort Elt to LocValuation . endv
view ClockVal from TRIV to VALUATION is sort Elt to ClockValuation . endv
view ParVal from TRIV to VALUATION is sort Elt to ParValuation . endv
view DVarVal from TRIV to VALUATION is sort Elt to DVarValuation . endv

view RVar from TRIV to ALT-REAL-INTEGER-EXPR  is sort Elt to RVar . endv

fmod SHARED-STATE is 
 pr VALUATION .
 pr SET{LocVal}   * (sort Set{LocVal} to SetLocValuation, sort NeSet{LocVal} to NeSetLocValuation) .
 pr SET{ClockVal} * (sort Set{ClockVal} to SetClockValuation, sort NeSet{ClockVal} to NeSetClockValuation) .
 pr SET{ParVal}   * (sort Set{ParVal} to SetParValuation, sort NeSet{ParVal} to NeSetParValuation) .
 pr SET{DVarVal}  * (sort Set{DVarVal} to SetDVarValuation, sort NeSet{DVarVal} to NeSetDVarValuation) .

 --- Can we do tick?
 sort TickState .
 ops tickOk tickNotOk : -> TickState [ctor] .

 --- the last n:Nat is for fresh variable generation
 sort SharedState .
 op < tick:_ locs:_ clocks:_ parameters:_ dvariables:_ constraint:_ n:_>
    : TickState SetLocValuation SetClockValuation SetParValuation SetDVarValuation BoolExpr Nat -> SharedState [ctor format( d n d n d n d n d n d n d n d n d)] .

endfm 

--- Simplification procedures using FME 
fmod SMT-INTERFACE is
 pr ALT-SMT-CHECK . 
 pr SHARED-STATE .
 pr SET{RVar}  * (sort Set{RVar} to SetVar, sort NeSet{RVar} to NeSetVar) .

 --- IDs for SMT variables 
 op tvar : Nat       -> SMTVarId [ctor] . --- ticks
 op var  : Parameter -> SMTVarId [ctor] .
 op var  : DVariable -> SMTVarId [ctor] .
 op var  : Clock     -> SMTVarId [ctor] .

 --- --------------------------------
 vars X Y        : RVar .
 var B1 B2       : BoolExpr .
 var Xs Xs'      : SetVar .
 var N           : Int .
 var ID          : SMTVarId .
 vars RE RE1 RE2 : RExpr .
 var CLOCK       : Clock .
 var CVALUES     : SetClockValuation .
 --- --------------------------------

 --- Extracting tvar variables from a ConjRelLinRExpr
 op tvars : BoolExpr -> SetVar .
 op tvars : RExpr    -> SetVar .

 eq tvars(true)  = empty .
 eq tvars(B1 and B2) = tvars(B1), tvars(B2) .

 eq tvars(rr(tvar(N))) = rr(tvar(N)) .
 eq tvars(RE1 <= RE2)  = tvars(RE1), tvars(RE2).
 eq tvars(RE1 < RE2)   = tvars(RE1), tvars(RE2).
 eq tvars(RE1 >= RE2)  = tvars(RE1), tvars(RE2).
 eq tvars(RE1 > RE2)   = tvars(RE1), tvars(RE2).

 eq tvars(RE1 + RE2) = tvars(RE1) , tvars(RE2) .
 eq tvars(RE1 - RE2) = tvars(RE1) , tvars(RE2) .
 eq tvars(RE1 * RE2) = tvars(RE1) , tvars(RE2) .
 eq tvars(RE1 / RE2) = tvars(RE1) , tvars(RE2) .

 eq tvars(RE1) = empty [owise] .
 eq tvars(B1) = empty [owise] .

 --- Extracting clock vars from ConjRelLinRExpr
 op cvars : BoolExpr -> SetVar .
 op cvars : RExpr    -> SetVar .

 eq cvars(true)  = empty .
 eq cvars(B1 and B2) = cvars(B1), cvars(B2) .

 eq cvars(rr(var(CLOCK))) = rr(var(CLOCK)) .
 eq cvars(RE1 <= RE2)  = cvars(RE1), cvars(RE2).
 eq cvars(RE1 < RE2)   = cvars(RE1), cvars(RE2).
 eq cvars(RE1 >= RE2)  = cvars(RE1), cvars(RE2).
 eq cvars(RE1 > RE2)   = cvars(RE1), cvars(RE2).

 eq cvars(RE1 + RE2) = cvars(RE1) , cvars(RE2) .
 eq cvars(RE1 - RE2) = cvars(RE1) , cvars(RE2) .
 eq cvars(RE1 * RE2) = cvars(RE1) , cvars(RE2) .
 eq cvars(RE1 / RE2) = cvars(RE1) , cvars(RE2) .

 eq cvars(RE1) = empty [owise] .
 eq cvars(B1) = empty [owise] .

 --- Variables in a clock expression 
 op vars-in-clocks : SetClockValuation -> SetVar .
 eq vars-in-clocks(empty) = empty .
 eq vars-in-clocks( (CLOCK : RE, CVALUES) ) = tvars(RE), vars-in-clocks(CVALUES) . 

 --- Eliminate a set of variable 
 op fme-all : SetVar BoolExpr -> BoolExpr .
 eq fme-all(empty, B1) = B1 .
 eq fme-all((X, Xs), B1) = fme-all(Xs, fme(X, B1)) .

 --- Eliminate all the tick variables
 op elim-ticks : BoolExpr -> BoolExpr .
 eq elim-ticks(B1) = fme-all(tvars(B1), B1) .

 --- Eliminate clock-variables and ticks
 op elim-clocks : BoolExpr -> BoolExpr .
 eq elim-clocks(B1) = fme-all(cvars(B1), B1) .
endfm

fmod STATE is 
 pr SMT-INTERFACE .

 --- Global state 
 sort State .
 op {_}_ : Network SharedState -> State [ctor] .
 op error : -> State . --- For meta-level operations

 --- ---------------------------------------------
 var N                              : Nat .
 var TS                             : TickState .
 vars L1 L2 L1' L2'                 : Location .
 var AI                             : AutoId . 
 vars C1 C2 CLOCK                   : Clock .
 vars RESET RESET1 RESET2           : Resets .
 vars G G1 G2 INV INV1 INV2         : Constraint .
 vars R1 R2                         : RExpr .
 vars R T                           : RExpr .
 vars P1 P2 PAR                     : Parameter .
 vars D1 D2 DVAR                    : DVariable .
 var TRANS TRANS1 TRANS2            : SetTransition .
 var CVALUES CVALUES'               : SetClockValuation .
 var DVVALUES DVVALUES'             : SetDVarValuation .
 var PVALUES PVALUES'               : SetParValuation .
 var LOCS LOCS'                     : SetLocValuation .
 var NET                            : Network .
 var SSTDEF SSTDEF1 SSTDEF2         : SetStateDef .
 vars CREXPR1 CREXPR2               : ClockRExpr .                        
 vars LINREXP1 LINREXP2             : PTA-LinRExpr .
 vars CRT1 CRT2                     : ClockRTerm .
 var LRT                            : PTA-LinRTerm .
 var CONS                           : BoolExpr .
 --- ---------------------------------------------

  --- --------------------
  --- Operations on states
  --- --------------------
  op get-network : State -> Network . 
  eq get-network({ NET } < tick: TS locs: LOCS clocks: CVALUES parameters: PVALUES dvariables: DVVALUES constraint: CONS n: N >) = NET .

  op get-state : State -> SharedState  . 
  eq get-state({ NET } < tick: TS locs: LOCS clocks: CVALUES parameters: PVALUES dvariables: DVVALUES constraint: CONS n: N >) = 
                       < tick: TS locs: LOCS clocks: CVALUES parameters: PVALUES dvariables: DVVALUES constraint: CONS n: N > .

  op make-state : Network SharedState -> State .
  eq make-state(NET, < tick: TS locs: LOCS clocks: CVALUES parameters: PVALUES dvariables: DVVALUES constraint: CONS n: N >) =
              { NET } < tick: TS locs: LOCS clocks: CVALUES parameters: PVALUES dvariables: DVVALUES constraint: CONS n: N > .

  --- Extracting the constraint from the state 
  op getConstraint : State -> BoolExpr .
  eq getConstraint((error).State) = false .
  eq getConstraint({ NET } < tick: TS locs: LOCS clocks: CVALUES parameters: PVALUES dvariables: DVVALUES constraint: CONS n: N > ) = CONS .

 --- --------------------
 --- Operations on clocks
 --- --------------------

 --- Increase all the clocks + T 
 op +time : RExpr SetClockValuation -> SetClockValuation .
 eq +time(T, empty) = empty .
 eq +time(T, (CLOCK : R, CVALUES)) = (CLOCK : (R + T)), +time(T, CVALUES) . 

 --- Resetting clocks 
 op reset-clocks : Resets SetClockValuation -> SetClockValuation .
 eq reset-clocks(nothing, CVALUES) = CVALUES .
 eq reset-clocks(( DVAR := R   ; RESET ),  CVALUES) =   reset-clocks(RESET, CVALUES) .
 eq reset-clocks(( CLOCK := 0 ; RESET ), (CLOCK : R1, CVALUES)) = (CLOCK : 0), reset-clocks(RESET, CVALUES) .

 --- Resetting discrete variables 
 op reset-dvars : Resets SetDVarValuation -> SetDVarValuation .
 eq reset-dvars(nothing, DVVALUES) = DVVALUES .
 eq reset-dvars( ( DVAR := R   ; RESET ),  (DVAR : R1 ,  DVVALUES)) = (DVAR : R),   reset-dvars(RESET, DVVALUES) .
 eq reset-dvars( ( CLOCK := 0   ; RESET ),  DVVALUES )  =  reset-dvars(RESET, DVVALUES) .

 --- Evaluating constraints into (SMT) BoolExpr
 op eval : Constraint SetClockValuation SetParValuation SetDVarValuation -> BoolExpr .
 op eval : DVariable  SetDVarValuation                                   -> RExpr .
 op eval : PTA-LinRTerm  SetParValuation                                 -> RExpr .
 op eval : ClockRExpr  SetClockValuation                                 -> RExpr .

 --- Constraints 
 eq eval(true, CVALUES, PVALUES, DVVALUES)         =  true .
 eq eval(G1 and G2, CVALUES, PVALUES, DVVALUES)    =  eval(G1, CVALUES, PVALUES, DVVALUES) and 
                                                      eval(G2, CVALUES, PVALUES, DVVALUES) .

 eq eval(CREXPR1 <= LINREXP1, CVALUES, PVALUES, DVVALUES) =  eval(CREXPR1, CVALUES) <= eval(LINREXP1, PVALUES) .
 eq eval(CREXPR1 <  LINREXP1, CVALUES, PVALUES, DVVALUES) =  eval(CREXPR1, CVALUES) <  eval(LINREXP1, PVALUES) .
 eq eval(CREXPR1 >= LINREXP1, CVALUES, PVALUES, DVVALUES) =  eval(CREXPR1, CVALUES) >= eval(LINREXP1, PVALUES) .
 eq eval(CREXPR1 >  LINREXP1, CVALUES, PVALUES, DVVALUES) =  eval(CREXPR1, CVALUES) >  eval(LINREXP1, PVALUES) .
 eq eval(CREXPR1 =  LINREXP1, CVALUES, PVALUES, DVVALUES) =  eval(CREXPR1, CVALUES) === eval(LINREXP1, PVALUES) .

 eq eval(LINREXP1 <= CREXPR1, CVALUES, PVALUES, DVVALUES) =  eval(CREXPR1, CVALUES) >= eval(LINREXP1, PVALUES) .
 eq eval(LINREXP1 <  CREXPR1, CVALUES, PVALUES, DVVALUES) =  eval(CREXPR1, CVALUES) >  eval(LINREXP1, PVALUES) .
 eq eval(LINREXP1 >= CREXPR1, CVALUES, PVALUES, DVVALUES) =  eval(CREXPR1, CVALUES) <= eval(LINREXP1, PVALUES) .
 eq eval(LINREXP1 >  CREXPR1, CVALUES, PVALUES, DVVALUES) =  eval(CREXPR1, CVALUES) <  eval(LINREXP1, PVALUES) .
 eq eval(LINREXP1 =  CREXPR1, CVALUES, PVALUES, DVVALUES) =  eval(CREXPR1, CVALUES) === eval(LINREXP1, PVALUES) .


 eq eval(DVAR =  R, CVALUES, PVALUES, DVVALUES)  =  eval(DVAR, DVVALUES) === R .
 eq eval(DVAR !=  R, CVALUES, PVALUES, DVVALUES)  =  eval(DVAR, DVVALUES) =/== R .
 eq eval(DVAR >  R, CVALUES, PVALUES, DVVALUES)  =  eval(DVAR, DVVALUES) > R .
 eq eval(DVAR <  R, CVALUES, PVALUES, DVVALUES)  =  eval(DVAR, DVVALUES) < R .
 eq eval(DVAR >=  R, CVALUES, PVALUES, DVVALUES)  =  eval(DVAR, DVVALUES) >= R .
 eq eval(DVAR <=  R, CVALUES, PVALUES, DVVALUES)  =  eval(DVAR, DVVALUES) <= R .

 --- Discrete Variables 
 eq eval(DVAR, ((DVAR : R), DVVALUES))   = R .

 --- ClockRExpr
 eq eval(CLOCK, (CLOCK : R, CVALUES)) = R .
 eq eval(0, CVALUES) = 0 .
 eq eval(CRT1 - CRT2, CVALUES) = eval(CRT1, CVALUES) - eval(CRT2, CVALUES) .

 --- Linear Terms
 eq eval(LINREXP1 + LINREXP2, PVALUES) = eval(LINREXP1, PVALUES) + eval(LINREXP2, PVALUES) .
 eq eval(LINREXP1 - LINREXP2, PVALUES) = eval(LINREXP1, PVALUES) - eval(LINREXP2, PVALUES) .
 eq eval(PAR, ((PAR : R), PVALUES))     = R .
 eq eval(R, PVALUES) = R .
 eq eval(R * LRT, PVALUES) = R * eval(LRT, PVALUES) .

 --- Extracting the invariants 
 op inv : Network SetLocValuation SetClockValuation SetParValuation SetDVarValuation -> BoolExpr . 
 eq inv(NET, empty, CVALUES, PVALUES, DVVALUES) = true .
 eq inv( (< AI | (@ L1 inv INV : TRANS ), SSTDEF >, NET), 
         ((AI @ L1) , LOCS) , CVALUES, PVALUES, DVVALUES) = 
         eval(INV, CVALUES, PVALUES, DVVALUES) and inv(NET, LOCS, CVALUES, PVALUES, DVVALUES) .
endfm
