# nptav2maude

We give a rewriting logic semantics for parametric timed automata with global
variables (PTAVs) and show that symbolic reachability analysis using
[Maude-with-SMT](https://maude-se.github.io/) is sound and complete for the PTA
reachability problem. We refine standard Maude-with-SMT reachability so that the
analysis terminates when the symbolic state space of the PTA is finite.  Besides
reachability, the rewrite theory can be also used for parameter synthesis. This
repository includes benchmarks comparing our methods and
[Imitator](https://imitator.lipn.univ-paris13.fr/).

## Getting started

The project was tested in [Maude 3.2.1](http://maude.cs.illinois.edu/) and [Maude
SE](https://maude-se.github.io/). A script written in [Python
3.10](https://www.python.org/) is used to parse Imitator input files into Maude
files.

### Maude files

The specification can be found in the folder `maude`. It includes the following files:

- _alt-smt_: Representation of SMT Boolean, Integer and Real expressions as
  Maude's terms. SMT variables are encoded as terms of the form `bb(x)`, `ii(x)`
  and `rr(x)` where `x` is a term of sort `SMTVarId`. 
- _analysis_: Definition of commands to perform parameter-synthesis and search
  with folding. 
- _fme_: Implementation of the Fourier–Motzkin elimination procedure. 
- _semantics_: Rewriting rules defining the semantics of networks of PTAs
- _state_: Sorts and constructors for defining the state of the NPTAD. 
- _syntax_: Syntax for defining NPTADs. 

See the headers of each file for further information.

### Parser

The parser of Imitator models into Maude theories can be found in `nptav2maude`.
See the file [README](./nptav2maude/README.md) for further information about its
implementation in Python.

### Benchmarks

See the file [README](./benchmarks/README.md) for further information about
the benchmarks comparing the performance of Imitator and our analyses.
