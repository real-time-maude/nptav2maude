import os

import pytest
from src.Parser import Parser


def remove_spaces(output_str):
    """Removes all the spaces in a string"""
    output = list(filter(None, [e.strip() for e in output_str.split("\n")]))
    return output


@pytest.mark.parametrize(
    "imitator_file,maude_file,method",
    [
        ("coffee.imi", "coffee.maude", "no-folding"),
        ("coffee.imi", "coffee-folding.maude", "folding"),
        ("coffee.imi", "coffee-intrpr.maude", "search"),
        ("fischer.imi", "fischer.maude", "no-folding"),
        ("train-intruder.imi", "train-intruder.maude", "no-folding"),
    ],
)
def test_parser(examples_dir, imitator_file, maude_file, method):
    parser = Parser()

    # parse the imitator file
    input_file = os.path.join(examples_dir, imitator_file)
    output = parser.parse_file(input_file, method)

    # read the file with the expected output
    expected_file = os.path.join(examples_dir, maude_file)
    expected = open(expected_file).read()

    assert remove_spaces(output) == remove_spaces(expected)
