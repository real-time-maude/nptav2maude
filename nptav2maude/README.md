# NPTAV2Maude

This repository contains a rewriting logic specification for the verificaton of
Parametric Timed Automata with global variables (NPTAV) models.

## Dependencies

The tool was written in [`Python3`](https://www.python.org/downloads/) and the
dependencies can be installed using the following command.

```
pip3 install -r requirements.txt
```

If the parser and lexer files need to be generated, you can run the following
command in the `src` folder, once you have installed [antlr](https://www.antlr.org/).

```
antlr -Dlanguage=Python3 -no-listener -visitor -o dist Imitator.g4
```

## Getting Started

It takes as input an [`Imitator`](https://www.imitator.fr/) model and returns an
[`Maude`](http://maude.cs.illinois.edu/w/index.php/The_Maude_System)
specification.

```
λ> python3 app.py -h
usage: app.py [-h] [--output OUTPUT] --input INPUT [--folding]

From Imitator to Maude specifications

options:
  -h, --help       show this help message and exit
  --output OUTPUT  Maude output file (default: ./model.maude)
  --input INPUT    Imitator file (default: None)
  --folding        Flag to use folding (default: False)
```

Next, an example of use:

```
python3 app.py --input tests/examples/coffee.imi
```

## Running Test

A set of tests can be run with the following command:

```
python3 -m pytest -vv
```
