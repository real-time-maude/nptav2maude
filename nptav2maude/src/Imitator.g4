/**
 * This file defines the syntax for parsing IMITATOR models into maude
 * specifications.
 */
grammar Imitator;

// ------------------------------------------------------------------------
//  Parser Rules
// ------------------------------------------------------------------------

// Main
main               : declarations? automaton* initial_state? END;

// variable declarations
declarations       : VAR var_definition+;
var_definition     : clock_definitions | param_definition | discrete_definition;
clock_definitions  : name_list COLON CLOCK SEMI;
param_definition   : const_name_list COLON PARAMETER SEMI;
discrete_definition: name_list COLON DISCRETE SEMI;
name_list          : NAME (COMMA NAME)* COMMA?;
const_or_name      : NAME | NAME OP_EQ NUMBER;
const_name_list    : const_or_name (COMMA const_or_name)* COMMA?;


// initial state
initial_state      : INIT ASSIGN initial_location* initial_constraint* SEMI;
initial_location   : AMPERSAND LOC LSQBRA auto=NAME RSQBRA OP_EQ loc=NAME;
initial_constraint : AMPERSAND expr;

// automaton
automaton          : AUTOMATON NAME synclabs? locations* END;
synclabs           : SYNCLABS COLON name_list? SEMI;
locations          : location_type? LOC NAME COLON invariant transition*;
invariant          : INVARIANT predicate;
location_type      : (URGENT)? ACCEPTING | (ACCEPTING)? URGENT ;

// transition
transition         : WHEN predicate action? GOTO NAME SEMI;
action             : updates sync_label? | sync_label updates?;
sync_label         : SYNC NAME;

// updates
update             : NAME ASSIGN expr;
update_list        : update (COMMA update)*;
updates            : DO LBRACE update_list? RBRACE;

// predicates & expresions
predicate          : expr;
expr               : LPAR expr RPAR
                   | NOT expr
                   | left=expr bool_op right=expr
                   | expr binary_op expr
                   | TRUE
                   | FALSE
                   | NAME
                   | NUMBER
                   ;

// binary operators
binary_op          : OP_EQ | OP_NEQ | OP_G | OP_GEQ | OP_L | OP_LEQ;

// boolean operators
bool_op            : AMPERSAND | PIPE;

// ------------------------------------------------------------------------
// Lexer Rules
// ------------------------------------------------------------------------

// keywords
AUTOMATON          : 'automaton';
ACCEPTING          : 'accepting';
CLOCK              : 'clock';
CONSTANT           : 'constant';
DISCRETE           : 'discrete';
DO                 : 'do';
END                : 'end';
GOTO               : 'goto';
INIT               : 'init';
INVARIANT          : 'invariant';
LOC                : 'loc';
PARAMETER          : 'parameter';
SYNC               : 'sync';
SYNCLABS           : 'synclabs';
URGENT             : 'urgent';
VAR                : 'var';
WHEN               : 'when';

// comments & new lines
COMMENT            : '(*' .*? '*)' -> skip;
NEWLINE            : ('\r'? '\n' | '\r')+ -> skip;
WHITESPACE         : (' ' | '\t')+ -> skip;

// boolean values
FALSE              : 'False';
TRUE               : 'True';

// symbols
AMPERSAND          : '&';
PIPE               : '|';
LBRACE             : '{';
RBRACE             : '}';
LPAR               : '(';
RPAR               : ')';
LSQBRA             : '[';
RSQBRA             : ']';
SEMI               : ';';
COLON              : ':';
COMMA              : ',';

// operators
ASSIGN             : ':=';
NOT                : 'not';
OP_EQ              : '=';
OP_L               : '<';
OP_G               : '>';
OP_GEQ             : '>=';
OP_LEQ             : '<=';
OP_NEQ             : '<>';

// numbers
NUMBER             : NAT | FLOAT | RATIONAL | INT ;
NAT                : [0-9]+ ;
INT                : '-'? NAT ;
FLOAT              : INT '.' NAT ;
RATIONAL           : NAT '/' NAT;

// variable names
NAME               : '_'* [A-Za-z][A-Za-z0-9_]*;