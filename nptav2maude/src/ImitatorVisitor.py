from src.dist.ImitatorVisitor import ImitatorVisitor
from src.Imitator import Automaton, Location, Model, Transition


class MyVisitor(ImitatorVisitor):
    """
    Class extending the visitor of Imitator models

    Attributes
    ----------
    model : Model
        Imitator model
    fresh_clocks : dict[str]
        Dictionary storing the fresh clocks of a location
    """

    def __init__(self):
        super().__init__()
        self.model = Model()
        self.fresh_clocks = {}

    def _add_fresh_clock(self, location_name: str):
        """Add a new fresh clock for a location
        TODO: check if prefix for fresh clocks is not already taken
        """
        if location_name in self.fresh_clocks:
            return self.fresh_clocks[location_name]

        fresh_clock = f"u{len(self.fresh_clocks)}"
        self.model.add_clock(fresh_clock)
        self.fresh_clocks[location_name] = fresh_clock

        return fresh_clock

    def _sanitize_location_name(self, location_name: str):
        name = str(location_name).replace("_", "")
        return name

    def visitMain(self, ctx):
        self.visitChildren(ctx)
        return self.model

    def visitClock_definitions(self, ctx):
        clocks = ctx.name_list().getText().split(",")
        clocks = [c for c in clocks if len(c)]
        self.model.add_clock(clocks)

        return self.visitChildren(ctx)

    def visitDiscrete_definition(self, ctx):
        variables = ctx.name_list().getText().split(",")
        variables = [v for v in variables if len(v)]
        self.model.add_discrete_var(variables)

        return self.visitChildren(ctx)

    def visitParam_definition(self, ctx):
        parameters = ctx.const_name_list().getText().split(",")
        parameters = [p for p in parameters if len(p)]
        for p in parameters:
            if "=" in p:
                self.model.add_initial_constraint(p)
                p = p.split("=")[0]
            self.model.add_parameter(p)

        return self.visitChildren(ctx)

    def visitAutomaton(self, ctx):
        synclabs = []
        parsed_synclabs = ctx.synclabs()
        if parsed_synclabs is not None and parsed_synclabs.name_list() is not None:
            synclabs = parsed_synclabs.name_list().getText().split(",")
            synclabs = [self._sanitize_location_name(s) for s in synclabs]

        automaton = Automaton(self._sanitize_location_name(ctx.NAME()), synclabs)
        locations = ctx.locations()

        # first parse locations
        for location in locations:
            invariant = location.invariant().predicate().getText()
            source = self._sanitize_location_name(location.NAME())

            # parse types of the location
            location_types = []
            if location.location_type() is not None:
                location_types = [
                    loc_t.getText() for loc_t in location.location_type().getChildren()
                ]

            accepting = "accepting" in location_types
            urgent = "urgent" in location_types

            if urgent:
                fresh_clock = self._add_fresh_clock(source)

                # add invariant
                invariant = f"{invariant}&" if invariant != "True" else ""
                invariant += f"{fresh_clock}<=0"

            automaton.add_location(Location(source, invariant, False, accepting, False))

        # first parse transitions
        for location in locations:
            source = self._sanitize_location_name(location.NAME())
            for transition in location.transition():
                guard = transition.predicate().getText()
                target = self._sanitize_location_name(transition.NAME())

                action = transition.action()
                update = []
                sync = None
                if action is not None:
                    if action.updates() is not None:
                        update_list = action.updates().update_list()
                        if update_list is not None:
                            update = update_list.getText().split(",")

                    sync = action.sync_label()
                    if sync is not None:
                        sync = self._sanitize_location_name(
                            str(action.sync_label().NAME())
                        )

                # add reset if transition points to an urgent location
                if target in self.fresh_clocks:
                    fresh_clock = self.fresh_clocks[target]
                    update.append(f"{fresh_clock}:=0")

                automaton.add_transition(
                    Transition(source, guard, update, sync, target)
                )

        self.model.add_automaton(automaton)
        return self.visitChildren(ctx)

    def visitInitial_location(self, ctx):
        automaton = str(ctx.auto.text)
        location = self._sanitize_location_name(ctx.loc.text)
        self.model.get_automaton(automaton).get_location(location).initial = True

        return self.visitChildren(ctx)

    def visitInitial_constraint(self, ctx):
        constraints = ctx.expr().getText().split("&")
        self.model.add_initial_constraint(constraints)

        return self.visitChildren(ctx)
