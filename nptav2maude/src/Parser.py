import re
import textwrap

import antlr4
from src.dist.ImitatorLexer import ImitatorLexer
from src.dist.ImitatorParser import ImitatorParser
from src.Imitator import Automaton, Model, Transition
from src.ImitatorVisitor import MyVisitor
from src.Maude import ImportType, Maude


class Parser:
    """A class to represent the parser of Imitator to Maude using the interpreter version"""

    def __init__(self) -> None:
        """Constructor of the class"""
        self.maude = Maude()

    def _get_parameter_constraints(self, model) -> list[str]:
        """Get the initial constraints of parameters

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        list[str]
        """
        params = model.parameters
        params_constraints = []
        for c in model.initial_constraints:
            v1, op, v2 = self._normalize_constraints(c, False, True).split(" ")
            new_v1 = f"rr(var({v1}))" if v1 in params else v1
            new_v2 = f"rr(var({v2}))" if v2 in params else v2

            if new_v1.startswith("rr(") or new_v2.startswith("rr)"):
                params_constraints.append(f"{new_v1} {op} {new_v2}")

        return params_constraints

    def _get_discrete_constraints(self, model) -> list[str]:
        """Get the initial constraints of discrete variables

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        list[str]
        """
        vars = model.discrete_vars
        var_constraints = []
        for c in model.initial_constraints:
            v1, _, v2 = self._normalize_constraints(c, False).split(" ")
            if v1 in vars:
                var_constraints.append(f"{v1} : {v2}")

        return var_constraints

    def _normalize_float(self, number: str) -> str:
        """Transforms float into rational"""
        new_number = number.replace(".", "")
        nb_decimals = len(new_number) - number.index(".")
        new_number += f"/1{'0'*nb_decimals}"

        return new_number.lstrip("0")

    def _normalize_numbers(self, constraint: str) -> str:
        """Add x/1 to the numbers in a constraint"""
        number = re.sub(r"((?<!\.|\/)\b[0-9]+\b(?!\.|\/))", r"\1/1", constraint)
        float = re.sub(
            r"(\d+\.\d+)", lambda g: self._normalize_float(g.group(1)), number
        )

        return float

    def _normalize_constraints(
        self, constraint: str, parens: bool = True, expand_equal: bool = False
    ) -> str:
        """
        Normalize the string represention of a constraint. For instance, adding
        spaces around binary operators.

        Parameters
        ----------
        constraint : str
            string representation of the constraint
        parens : bool
            Flag to surround constraint with parenthesis
        expand_equal : bool
            Flag to use the maude symbol for equal

        Returns
        -------
        str
        """
        # replace True by true
        new_constr = "true" if constraint == "True" else constraint

        # add space around binary operators
        c_with_spaces = re.sub(r"((?!\.|\/)[^\w-]+)", r" \1 ", new_constr)

        # remove more than one space
        no_spaces = re.sub(" +", " ", c_with_spaces)

        # add x/1 to all the number
        fix_numbers = self._normalize_numbers(no_spaces)

        # replace binary & by and
        result = fix_numbers.replace(" | ", " or ").replace(" & ", " and ")

        if expand_equal:
            result = result.replace(" = ", " === ")

        if parens and result != "true":
            c_list = [f"({g})" for g in result.split(" and ")]
            result_str = " and ".join(c_list)
            result = f"({result_str})" if len(c_list) > 1 else result_str

        return result

    def _get_preamble(self, model: Model) -> str:
        """
        Get the preamble of the Maude file

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        str
        """
        # locations
        locations = sorted(
            [loc.name for automaton in model.automata for loc in automaton.locations]
        )
        locations_str = self.maude.operators(locations, [], "Location", ["ctor"])

        # actions
        actions = sorted(model.actions + ["noaction"])
        actions_str = self.maude.operators(actions, [], "Action", ["ctor"])

        # automata label
        names = sorted([automaton.name for automaton in model.automata])
        names_str = self.maude.operators(names, [], "AutoId", ["ctor"])

        # clocks
        clocks = sorted(model.clocks)
        clocks_str = self.maude.operators(clocks, [], "Clock", ["ctor"])

        # parameters
        parameters = sorted(model.parameters)
        parameters_str = self.maude.operators(parameters, [], "Parameter", ["ctor"])

        # discrete variables
        discrete_vars = sorted(model.discrete_vars)
        discrete_str = self.maude.operators(discrete_vars, [], "DVariable", ["ctor"])

        preamble = f"""
            {locations_str}
            {actions_str}
            {names_str}
            {clocks_str}
            {discrete_str}
            {parameters_str}"""

        # remove empty lines
        preamble = "\n".join([s for s in preamble.split("\n") if s.strip()])
        return preamble.strip()

    def _get_init_eq(self, model: Model) -> str:
        """Get the initial state of the Maude interpreter

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        str
        """
        label = "init"
        init_op = self.maude.operators([label], [], "State", [])

        # locs
        locs = [f"{a.name} @ {a.initial_location.name}" for a in model.automata]
        locs_str = f"({', '.join(locs)})"

        # clocks
        # TODO: check this with Carlos
        clocks = [f"{clock} : 0" for clock in model.clocks]
        clocks_str = f"({', '.join(clocks)})" if len(clocks) else "empty"

        # params
        # TODO: check this with Carlos
        params = [f"{param} : rr(var({param}))" for param in model.parameters]
        params_str = f"({', '.join(params)})" if len(params) else "empty"

        # variables
        variables = self._get_discrete_constraints(model)
        vars_str = f"({', '.join(variables)})" if len(variables) else "empty"

        # constraints
        constraints = self._get_parameter_constraints(model)
        constraints_str = " and ".join(constraints)

        tab = " " * 22
        config = f"\n{tab}".join(
            [
                "tick: tickOk",
                f"  locs: {locs_str}",
                f"  clocks: {clocks_str}",
                f"  parameters: {params_str}",
                f"  dvariables: {vars_str}",
                f"  constraint: {constraints_str}",
                "  n: 1",
            ]
        )

        agents = sorted([automaton.name.title() for automaton in model.automata])
        agents_str = ", ".join(agents)
        init_eq_str = f"eq {label} = {{ {agents_str} }}\n{tab}< {config} >\n{tab}."

        eq = f"""
            {init_op}
            {init_eq_str}"""

        return eq

    def _get_transition(self, t: Transition, sync_actions: list[str]) -> str:
        """Get the enconding of the transition of an automaton

        Parameters
        ----------
        t : Transition
            automaton transition
        sync_actions : list[str]
            list of actions that synchronise in the automata

        Returns
        -------
        str
        """
        # updates
        updates = " ; ".join(
            [re.sub(r"(\S+)\s*:=\s*(\S+)", r"\1 := \2", u) for u in t.update]
        )
        updates_str = "nothing" if updates == "" else updates

        # sync
        actions = t.sync
        if actions is None:
            sync_str = "local noaction"
        else:
            sync_str = (
                f"sync {actions}" if actions in sync_actions else f"local {actions}"
            )

        # guard
        guard = self._normalize_constraints(t.guard)

        return f"when {guard} {sync_str} do {{ {updates_str} }} goto {t.target}"

    def _get_automaton(self, automaton: Automaton, sync_actions: list[str]) -> str:
        """Get the encoding of an automaton in the Maude interpreter

        Parameters
        ----------
        automaton : Automaton
            Imitator model
        sync_actions : list[str]
            list of actions that synchronise in the automata

        Returns
        -------
        str
        """
        name = automaton.name
        tab = " " * 26

        op = self.maude.operators([name.title()], [], "Automaton", [])

        # add states with their transitions
        locations = []
        for loc in automaton.locations:
            trans = [
                self._get_transition(t, sync_actions)
                for t in automaton.transitions_from(loc.name)
            ]

            trans_str = f" ,\n{tab}     ".join(trans)
            trans_str = f"\n{tab}    ({trans_str})" if len(trans) else "empty"

            inv = self._normalize_constraints(loc.invariant)
            loc = f"(@ {loc.name} inv {inv} : {trans_str})"
            locations.append(loc)
        locations_str = f",\n{tab} ".join(locations)

        eq_str = f"eq {name.title()} = < {name} |\n{tab} {locations_str} >\n{tab}."

        result = f"""
            {op}
            {eq_str}"""

        return result

    def _get_search_cmd(self, nb_traces: int = None, nb_steps: int = None) -> str:
        """Return the command for parameter search

        Parameters
        ----------
        nb_traces : int
            number of traces to be found in the synthesis analysis
        nb_steps : int
            number of steps of the synthesis analysis
        """
        nb_traces_str = "" if (nb_traces is None) else f" [{nb_traces}]"
        nb_steps_str = "*" if (nb_steps is None) else f"{nb_steps}"

        cmd = f"""
        search{nb_traces_str} init =>{nb_steps_str} {{ NET:Network }} < tick: tickNotOk
                                            locs: (<replace>, LOCS:SetLocValuation)
                                            clocks: CLOCKS:SetClockValuation
                                            parameters: PARAMS:SetParValuation
                                            dvariables: DVARS:SetDVarValuation
                                            constraint: C:BoolExpr
                                            n: N:Nat >
                                            .
        """
        return cmd

    def _get_synthesis_cmd(self, nb_traces: int = None, nb_steps: int = None) -> str:
        """Return the command for parameter synthesis

        Parameters
        ----------
        nb_traces : int
            number of traces to be found in the synthesis analysis
        nb_steps : int
            number of steps of the synthesis analysis
        """
        params = []
        if nb_traces is not None:
            params.append(str(nb_traces))
        if nb_steps is not None:
            params.append(str(nb_steps))

        params = f"[ {' ; '.join(params)} ]"

        cmd = f"red synthesis {params} in 'MODEL : init => (<replace>) ."
        return cmd

    def _get_folding_cmd(self, nb_traces: int = None, nb_steps: int = None) -> str:
        """Return the command for reachability analysis

        Parameters
        ----------
        nb_traces : int
            number of traces to be found in the synthesis analysis
        nb_steps : int
            number of steps of the synthesis analysis
        """
        params = []
        if nb_traces is not None:
            params.append(str(nb_traces))
        if nb_steps is not None:
            params.append(str(nb_steps))

        params = f"[ {' ; '.join(params)} ]"

        cmd = f"red search-folding {params} in 'MODEL : init => (<replace>) ."
        return cmd

    def to_maude(self, model: Model, method: str = "search") -> str:
        """Parse an Imitator model into Maude

        Parameters
        ----------
        model : Model
            Imitator model
        method : string
            search method

        Returns
        -------
        str
        """
        initial_constraints = model.initial_constraints
        if len(initial_constraints):
            print("Please check the initial constraints in the search command:")
            print(f"    {initial_constraints}")

        load = self.maude.loadFile("analysis")

        module = "NPTAD-INTERPRETER" if method == "search" else "ANALYSIS"
        import_dynamics = self.maude.importModule(module, ImportType.EXTENDING)
        preamble = self._get_preamble(model)

        init = self._get_init_eq(model)

        actions = [
            action for a in model.automata for action in {t.sync for t in a.transitions}
        ]
        sync_actions = list(set([a for a in actions if actions.count(a) > 1]))

        equations = "\n".join(
            [self._get_automaton(a, sync_actions) for a in model.automata]
        )

        # choose the good command
        if method == "search":
            cmd = self._get_search_cmd(1)
        elif method == "folding":
            cmd = self._get_folding_cmd(1)
        elif method == "no-folding":
            cmd = self._get_synthesis_cmd(1)
        else:
            raise RuntimeError(f"Command {cmd} is not supported.")

        model_str = f"""\
        {load}

        mod MODEL is
            {import_dynamics}

            {preamble}
            {equations}
            {init}
        endm

        {cmd}

        quit .

        eof
        """

        return textwrap.dedent(model_str).strip()

    def parse_file(self, filename: str, method: str) -> str:
        """
        Parse an Imitator file into a Maude model

        Parameters
        ----------
        filename : str
            Imitator file
        method : str
            search method

        Returns
        -------
        str
        """
        fs_in = antlr4.FileStream(filename, encoding="utf8")

        # lexer
        lexer = ImitatorLexer(fs_in)
        stream = antlr4.CommonTokenStream(lexer)

        # parser
        parser = ImitatorParser(stream)
        tree = parser.main()

        # evaluator
        visitor = MyVisitor()
        model = visitor.visit(tree)

        # parse to Maude
        output = self.to_maude(model, method)

        return output
