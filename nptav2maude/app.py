import argparse

from src.Parser import Parser


def main(input_filename, output_filename, method):
    """
    Parse an Imitator file

    Parameters
    ----------
    input_filename : str
        Imitator filename
    output_filename : str
        Path of the output file
    method : str
        search method
    """
    try:
        parser = Parser()
        output = parser.parse_file(input_filename, method)

        # write to file
        with open(output_filename, "w") as f:
            f.write(output)
            print(f"Output file: {output_filename}")
    except Exception as E:
        print(f"Error processing {input_filename}.\n {E}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""From Imitator to Maude specifications""",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--output", type=str, default="./model.maude", help="Maude output file"
    )

    parser.add_argument("--input", type=str, required=True, help="Imitator file")

    parser.add_argument(
        "--method",
        type=str,
        required=True,
        choices=["folding", "no-folding", "search"],
        help="search method",
    )

    args = parser.parse_args()
    main(args.input, args.output, args.method)
