import argparse
import re


def main(input_filename):
    output = f'{input_filename.replace(".imi", "")}_loc.txt'
    with open(input_filename, "r") as fin, open(output, "w") as fout:
        for line in fin.readlines():
            # find automaton
            found_automaton = re.search(r"^\s*automaton\s+(\w+)", line)
            if found_automaton is not None:
                automaton = found_automaton.group(1).strip()

            # find locations
            found_loc = re.search(r"\s*loc\s+(\w+)\s*:", line)
            if found_loc is not None:
                loc = found_loc.group(1).strip()
                fout.write(f"{automaton} {loc}\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""Get the locations of an Imitator model""",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument("--input", type=str, required=True, help="Imitator file")

    args = parser.parse_args()
    main(args.input)
